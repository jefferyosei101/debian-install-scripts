#!/bin/bash

[ -x "$(command -v doas)" ] && [ -e /etc/doas.conf ] && ld="doas"
[ -x "$(command -v sudo)" ] && ld="sudo"

$ld apt install adb fastboot android-sdk android-sdk-libsparse-utils android-sdk-platform-tools -yy

