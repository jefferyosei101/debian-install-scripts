#!/bin/bash

# use either doas or sudo... whichever one is configured
[ -x "$(command -v doas)" ] && [ -e /etc/doas.conf ] && ld="doas"
[ -x "$(command -v sudo)" ] && ld="sudo"

# declare colors
red='\033[0;31m'
nc='\033[0m'

# sources enabled?
src="$(grep ^deb-src /etc/apt/sources.list | wc -l)"

# directories
srcdir="$HOME/.local/src"
dir="$(pwd)"
# use either doas or sudo... whichever one is configured
[ -x "$(command -v doas)" ] && [ -e /etc/doas.conf ] && ld="doas"
[ -x "$(command -v sudo)" ] && ld="sudo"

# install gcc and make if not already installed
[ -x "$(command -v gcc)" ] || $ld apt install gcc -yy
[ -x "$(command -v make)" ] || $ld apt install make -yy

[ "$src" -lt 1 ] && printf "${red} enable source repos in /etc/apt/sources.list and run script again${nc}\n" \
&& exit 1 \
|| $ld apt install xorg xserver-xorg suckless-tools firefox-esr pcmanfm xwallpaper \
&& $ld apt build-dep i3 -yy \
&& [ -e "$srcdir" ] || mkdir -p $srcdir \
&& git clone https://github.com/Airblader/i3 $srcdir/i3-gaps \
&& mkdir $srcdir/i3-gaps/build \
&& cd $srcdir/i3-gaps/build \
&& meson --prefix /usr/local/ \
&& ninja \
&& $ld ninja install \
&& cd $dir
